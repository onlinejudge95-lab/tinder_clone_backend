import express from 'express';
import mongoose from 'mongoose';
import Cors from 'cors';
import cardSchema from './dbSchema.js';


// App config
const app = express()
const port = process.env.PORT || 8080
const dbUri = "mongodb+srv://tinder_client_user:hltmKw1Oyz2UtV9U@tinder-clone.24lfc.mongodb.net/tinderdb?retryWrites=true&w=majority"

// Middlewares
app.use(express.json());
app.use(Cors());

// DB config
mongoose.connect(dbUri, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true
});

// API endpoints
app.get("/", (req, res) => res.status(200).send("Hello world!"))
app.post("/tinder/card", (req, res) => {
    const dbCard = req.body;

    cardSchema.create(dbCard, (err, data) => {
        if (err) {
            res.status(500).send(err)
        } else {
            res.status(201).send(data)
        }
    })
});
app.get("/tinder/card", (req, res) => {
    cardSchema.find((err, data) => {
        if (err) {
            res.status(500).send(err)
        } else {
            res.status(200).send(data)
        }
    })
})

// Listener
app.listen(port, () => console.log(`Listening on localhost:${port}`));
